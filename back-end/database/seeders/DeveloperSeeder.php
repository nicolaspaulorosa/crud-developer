<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeveloperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('developer')->insert([
            [
                'nome' => 'Marcos',
                'sexo' => 'M',
                'idade' => 24,
                'hobby' => 'Cozinhar',
                'datanascimento' => '1996-01-21'
            ],
            [
                'nome' => 'Jurandir',
                'sexo' => 'M',
                'idade' => 22,
                'hobby' => 'Pescar',
                'datanascimento' => '1998-04-12'
            ],
            [
                'nome' => 'Steffany',
                'sexo' => 'F',
                'idade' => 25,
                'hobby' => 'Paintball',
                'datanascimento' => '1995-08-21'
            ],
            [
                'nome' => 'Mario',
                'sexo' => 'M',
                'idade' => 41,
                'hobby' => 'Voleibol',
                'datanascimento' => '1980-08-14'
            ],
            [
                'nome' => 'Thiago Santos',
                'sexo' => 'M',
                'idade' => 42,
                'hobby' => 'Voleibol',
                'datanascimento' => '1979-08-14'
            ]
        ]);
    }
}
